import os, sys, time
import json
from selenium import webdriver

os.environ["PATH"] += os.pathsep + os.getcwd()
download_path = "dataset/"

def main():
	searchtext = sys.argv[1]
	num_requested = int(sys.argv[2])
	number_of_scrolls = num_requested / 400 + 1

	if not os.path.exists(download_path + searchtext.replace(" ", "_")):
		os.makedirs(download_path + searchtext.replace(" ", "_"))
	tbs = 'tbs=itp:photo'
	url = f'https://www.google.com/search?q={searchtext}&source=Int&hl=en&' + tbs + '&tbm=isch'
	driver = webdriver.Firefox()
	driver.get(url)

	headers = {}
	headers['User-Agent'] = "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36"
	for _ in range(int(number_of_scrolls)):
		for __ in range(10):
			# multiple scrolls needed to show all 400 images
			driver.execute_script("window.scrollBy(0, 1000000)")
			time.sleep(0.4)
		# to load next 400 images
		time.sleep(1)
		try:
			driver.find_element_by_xpath("//input[@value='Show more results']").click()
		except Exception as e:
			print (f'Less images found: {e}')
			break

	images = driver.find_elements_by_xpath('//div[contains(@class,"rg_meta")]')
	print ("Total images:", len(images), "\n")
	img_count=0
	with open(download_path + searchtext.replace(" ", "_")+f'/{searchtext}.txt','w') as f:
		for img in images:
			img_count += 1
			img_url = json.loads(img.get_attribute('innerHTML'))["ou"]
			f.write(img_url+'\n')
	print("Images written {}".format(img_count))
if __name__ == "__main__":
	main()